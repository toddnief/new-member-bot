#require 'rubygems'
require 'sinatra'
require 'selenium-webdriver'
require 'json'
#require 'pry'
require 'net/http'
require 'uri'
#require 'dotenv'

class NewMemberBot < Sinatra::Base

  get '/' do
    "This is the New Member Bot"
end

  post '/' do

  	# load environment variables
  	# Dotenv.load

    # pull ZP url and email from params
    client_email = params['Client Email']
    zen_planner_profile_url = params['Profile URL'].to_s

    # pull Zen Planner password from ENV
    zen_planner_password = ENV["ZP_PASS"]

    # configure the driver to run in headless mode
    options = Selenium::WebDriver::Chrome::Options.new
    options.add_argument('--headless')
    driver = Selenium::WebDriver.for :chrome, options: options

    # navigate driver to Zen Planner and log in
    driver.navigate.to "https://studio.zenplanner.com/zenplanner/studio/index.html#/login"
    element = nil

    wait = Selenium::WebDriver::Wait.new(timeout: 10) # seconds
    wait.until { element = driver.find_element(name: "email") }

    element.send_keys "todd@southloopsc.com"

    driver.find_element(id: "idPassword").send_keys zen_planner_password

    button = driver.find_element(css: "button").click

    # make sure the page loads
    wait.until { element = driver.find_element(id: "js-search-tab") }

    # navigate to profile
    driver.navigate.to zen_planner_profile_url

    # make sure the page loads
    wait.until { element = driver.find_element(:xpath, '//*[@id="idSidebar"]/div[1]/div[2]/div[1]/a/img')}
    photo_url = element.attribute('src')

    if photo_url.include? "studio.zenplanner.com"
        photo_url = "https://southloopsc.com/wp-content/uploads/2020/02/No-Member-Picture.jpg"
    end

    driver.quit

    # send a post request to Zapier with email and image url
    url = URI("https://hooks.zapier.com/hooks/catch/1093047/odvu01x/")

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Post.new(url)
    request["content-type"] = 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'
    request["cache-control"] = 'no-cache'
    request.body = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"zen_planner_profile_url\"\r\n\r\n#{photo_url}\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"client_email\"\r\n\r\n#{client_email}\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--"

    response = http.request(request)
  end

end
